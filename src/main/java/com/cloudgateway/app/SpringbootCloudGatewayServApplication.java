package com.cloudgateway.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringbootCloudGatewayServApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCloudGatewayServApplication.class, args);
	}

}
